import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wishlist';
  //observable en un objeto
  // se observa la fecha y la hora del pc en tiempo real (se actualiza cada (1000) 1 seg)
  // tiene aplicaciones como por ejemplo posicionamiento de coordenadas gps, consulta a servidor, consulta de estado.
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  })
}
