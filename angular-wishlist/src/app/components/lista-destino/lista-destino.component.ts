import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';



@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [DestinosApiClient] // inyectando el api al constructor
})
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  //destinos: DestinoViaje[];
  constructor(
    public destinosApiClient:DestinosApiClient,
    private store: Store<AppState>
    ) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];

    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        if (d != null) {
          this.updates.push('Se eligió f: ' + d.nombre);
        }
    });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);
    /*this.destinosApiClient.suscribeOnChange((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push('se ha elegido a: '+ d.nombre);
      }
    })*/
  }

  ngOnInit(): void {
    /*redux
    //Muestra la actividad de agregado de tarjetas
    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push("Se eligió: " + d.nombre);
        }
      });*/
  }
  /*
  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    //console.log(new DestinoViaje(nombre,url));
    //console.log(this.destinos);
    return false;
  }*/
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje){
    //desmarcar todos los demas en en array de elegidos
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //se marca el elegido
    //d.setSelected(true);
    //Actualizacion 2
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //Actualizacion 3
    this.destinosApiClient.elegir(d);
    //d.setSelected(true);
  }

  getAll(){

  }

}
