import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
//importando animaciones
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  //invocacion de animación
  animations: [
    trigger('esFavorito',[
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito',[animate('3s')]),
      transition('estadoFavorito => estadoNoFavorito',[animate('1s')]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() position: number;
  //crea una etiqueta para que angular tome el col-md-4 cada vez que se agregue algo
  @HostBinding('attr.class') cssClass = 'col-md-4';
  //Valor de salida
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    //inicializando el eventemitter()
    this.clicked = new EventEmitter();
   }

  ngOnInit(): void {
  }

  ir(){
    //que destino fue clickeado
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    //Accion
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    //accion
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
