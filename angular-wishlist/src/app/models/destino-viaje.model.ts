export class DestinoViaje {
	
    private selected: boolean;
    public servicios: string[];
	id: any;
    //public ahorra seteo de propiedades
    constructor(
        public nombre: string,
        public imagenUrl: string,
        public votes: number = 0
        ) {
        this.servicios = ['Piscina','Desayuno'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }

    voteUp() {
		this.votes++;
	}
    voteDown() {
		this.votes--;
	}
}