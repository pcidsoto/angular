import {
    reducerDestinosViajes,
    DestinosViajesState,
    intializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction, ElegidoFavoritoAction, VoteUpAction, VoteDownAction
  } from './destinos-viajes-state.model';
  import { DestinoViaje } from './destino-viaje.model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        //action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });
  
    it('should reduce new item added', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('barcelona');
    });
    it('should reduce new favorite', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('madrid', 'url'));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(0);
      //console.log('->',newState.favorito);
      expect(newState.favorito.nombre).toEqual('madrid');
    });
    it('should reduce new vote Up', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: VoteUpAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url',12));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      //console.log('->',action);
      //console.log('->',newState);
      expect(newState.items[0].votes).toEqual(12);
    });
    it('should reduce new vote down', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: VoteDownAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url',-12));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      //console.log('->',action);
      //console.log('->',newState);
      expect(newState.items[0].votes).toEqual(-12);
    });
  });