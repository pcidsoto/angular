describe('ventana principal', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4200');
      })
    

    it('tiene encabezado correcto', () => {
        cy.contains('angular-wishlist');
        cy.get('h1').should('contain', 'Ruteo Simple');
    });

    it('tiene el form encabezado correcto', () => {
        cy.contains('angular-wishlist');
        cy.get('label').should('contain', 'Nombre');
        cy.get('label').should('contain', 'Imagen Url');
    });

    it('Se agrega destino correcto', () => {
        cy.get('#nombre').type('destino 1').should('have.value', 'destino 1')
        cy.get('#imagenUrl').type('url destino 1').should('have.value', 'url destino 1')
        cy.get('.btn').click() 
    });

    it('El destino preferido es correcto', () => {
        cy.get('.btn-card').click() 
        cy.get('#preferido').should('contain', 'PREFERIDO');
    });
});