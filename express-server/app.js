/*
var express = require("express"); // requiriendo express
var app = express(); // inicializando el servidor
app.use(express.json()); // usaremos json
app.listen(3000, () => console.log("Server running on port 3000")); // diciendo que estara en el puerto 3000


app.get("/url", (req, res, next) => res.json(["Paris", "Barcelona","Montevideo" ]));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos = req.body;
    res.json(misDestinos);
});*/

var express = require("express"), cors= require('cors'); // requiriendo express
var app = express(); // inicializando el servidor
app.use(express.json()); // usaremos json
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000")); // diciendo que estara en el puerto 3000

var ciudades = ["Paris", "Barcelona", "Montevideo", "Santiago de Chile", "Mexico DF", "Nueva York"];

app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1 )));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});